<?php
/**
 * Plugin Name: Page Duplicator
 * Description: Duplicate a page by filling in certain words and phrases
 *
 * Author: Michael Loeffler
 * Version: 1.0.0
 *
 * @author Michael Loeffler <michael@michaelloeffler.com>
 */

require_once('core/duplicator.php');
require_once('admin/hook.php');

//$duplicator = new Duplicator(2);
/*
$duplicator->duplicateAll(array(
    'title' => 'Sample Page Duplicate',
    'name' => 'sample-page-duplicate',
    'status' => 'publish'
));
*/
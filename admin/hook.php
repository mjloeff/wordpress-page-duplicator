<?php

/**
 * Hook for admin menu
 */
function pd_admin_menu()
{
    add_menu_page('Page Duplicator', 'Page Duplicator', 'manage_options', 'page-duplicator-settings', pd_menu); //, $icon_url, $position );
}

add_action('admin_menu', 'pd_admin_menu');

/**
 * Get all pages
 *
 * @param String $postStatus Post status type(s) - e.g. publish,private,draft
 */
function pd_getPages($postStatus = 'publish,private,draft')
{
    $args = array(
        'sort_order' => 'asc',
        'sort_column' => 'post_id',
        'post_status' => $postStatus,
        'post_type' => 'page'
    );
    //echo 'get_pages=' . print_r(get_pages($args), true);
    return get_pages($args);
}

/**
 * Admin menu page
 */
function pd_menu()
{
    if (!empty($_POST) && check_admin_referer('pd-run', 'pd-admin-nonce')) {
        if (isset($_POST['run-button'])) {
            //echo 'Run button clicked';
            $duplicator = new Duplicator(intval($_POST['pd-source-page-id']));
            $cnt = $duplicator->duplicateAll(array(
                //'title' => 'Sample Page Duplicate',
                //'name' => 'sample-page-duplicate',
                'sitemap-prefix' => sanitize_text_field($_POST['pd-sitemap-prefix']),
                'keyword' => sanitize_text_field($_POST['pd-keyword']),
                'status' => 'publish',
                'tag1' => sanitize_text_field($_POST['pd-tag1']),
                'value1' => explode("\r\n", sanitize_textarea_field($_POST['pd-value1']))
            ));
            //echo $duplicator->test();
            ?>
            <div class="wrap">
            <?php
            if ($cnt > 0) {
                ?>
                <h1>Duplication was successful to <?php echo $cnt; ?> page(s)</h1>
                <?php
                //echo "value1=" . $_POST['pd-value1'];
            } else {
                ?>
                <h1>No duplication was performed - source page not chosen or duplication failed</h1>
                <?php
            }
        }
    }
    ?>
    <div class="wrap">
        <h1>Page Duplicator</h1>
        <!-- <form method="post" action="options.php"> -->
        <form method="post" action="options-general.php?page=page-duplicator-settings">
        <?php wp_nonce_field('pd-run', 'pd-admin-nonce' ); ?>
        <?php //settings_fields( 'pd-settings' ); ?>
        <?php //do_settings_sections( 'pd-settings' ); ?>
        <input type="hidden" value="run" name="run-button">
        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    Source page:
                </th>
                <td>
                    <select name="pd-source-page-id">
                        <?php foreach (pd_getPages() as $page) {
                        ?>
                        <option value="<?php echo $page->ID; ?>"><?php echo sanitize_title($page->post_title); ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <!-- <input type="text" name="pd-source-page" value="<?php echo get_option( 'pd-source-page' ); ?>"/> -->
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">
                    Page sitemap prefix (if blank, we won't create a sitemap)
                </th>
                <td>
                    <input type="text" name="pd-sitemap-prefix" value="service-area-"/>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">
                    Keyword (the topic)
                </th>
                <td>
                    <input type="text" name="pd-keyword" value="seo"/>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">
                    Tag (what to find)
                </th>
                <td>
                    <input type="text" name="pd-tag1" value="<?php echo get_option( 'pd-tag1' ); ?>"/>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">
                    Value(s) (what to replace - you may list multiple lines)
                </th>
                <td>
                    <textarea rows="10" cols="100" name="pd-value1"><?php echo get_option( 'pd_value1' ); ?></textarea>
                </td>
            </tr>
        </table>
        <?php submit_button('Duplicate!'); ?>
        </form>
    </div>
    <?php
}
<?php
/**
 * duplicator.php
 */
class Duplicator
{
    protected $sourcePageId = null;
    protected $sourcePage = null;
    protected $testData = null;

    public function __construct($sourcePageId = null)
    {
        $this->sourcePageId = $sourcePageId;
    }

    /**
     * Duplicate source to new page(s)
     *
     * @param Array $destination Destination info
     *
     * @return Integer Number of pages created
     */
    public function duplicateAll($destination)
    {
        try {
            $dest = is_array($destination) ? $destination : [];
            $cnt = 0;
            $this->sourcePage = $this->sourcePageId === null ? null : get_page($this->sourcePageId, OBJECT, 'raw');
            if ($this->sourcePage !== null) {
                $createSitemap = (array_key_exists('sitemap-prefix', $dest) && strlen(trim($dest['sitemap-prefix'])));

                if ($createSitemap) {
                    $sitemapPageId = $this->createSitemapPage($dest);
                }

                $pageIds = array();
                foreach ($dest['value1'] as $value1) {
                    if (!empty(trim($value1))) {
                        $pageId = $this->duplicateToPage($dest, array('tag' => $dest['tag1'], 'value' => $value1), $sitemapPageId);
                        if ($pageId !== null) {
                            array_push($pageIds, $pageId);
                            $cnt++;
                        }
                    }
                }

                if ($createSitemap) {
                    $this->updateSitemapPage($sitemapPageId, $pageIds);
                }
            }
        } catch (Exception $e) {
            return 0;
        }

        return $cnt;
    }

    /**
     * Duplicate source to new page
     *
     * @param Array        $destination  Destination info
     * @param Array        $tagValue     tag/value pair
     * @param Integer|null $parentPostId Parent post id or null if none
     *
     * @return Integer|null Page id created or null if failed
     */
    public function duplicateToPage($destination, $tagValue, $parentPostId = null)
    {
        try {
            $isCopyMetaData = false;  // Should we copy meta data?  Currently not working well with Beaver Builder, so I've disabled it

            $dest = is_array($destination) ? $destination : [];
            $destTitle = array_key_exists('keyword', $dest) ? $dest['keyword'] . '-' . $tagValue['value'] : $this->sourcePage->post_title;
            $destPageName = array_key_exists('keyword', $dest) ? $dest['keyword'] . '-' . $tagValue['value'] : $this->sourcePage->post_name;
            $destStatus = array_key_exists('status', $dest) ? $dest['status'] : 'publish';
            $destExcerpt = $this->sourcePage->post_excerpt;

            //$checkDest = get_page_by_title($destTitle, OBJECT, 'page');  // @todo  2019-10-06  MJL  Option to skip creation of page by title?
            $checkDest = null;
            if (empty($checkDest)) {
                $this->testData = 'post_content (raw)=' . $this->sourcePage->post_content . ', filter=' . apply_filters('the_content', $this->sourcePage->post_content);
                $newContent = $this->replaceTagWithValue($tagValue['tag'], $tagValue['value'], $this->sourcePage->post_content);
                //$newContent = $this->replaceTagWithValue($tagValue['tag'], $tagValue['value'], apply_filters('the_content', $this->sourcePage->post_content));

                //$postMeta = get_post_meta($this->sourcePageId);
                $pageId = wp_insert_post(array(
                    'ID' => 0,
                    'comment_status' => 'closed',
                    'ping_status'    => 'closed',
                    //'post_author'    => 1,
                    'post_title'     => $this->replaceTagWithValue($tagValue['tag'], $tagValue['value'], $destTitle),
                    'post_name'      => sanitize_title_with_dashes($this->replaceTagWithValue($tagValue['tag'], $tagValue['value'], $destPageName), null, 'save'),
                    'post_status'    => $destStatus,
                    'post_content'   => $newContent,
                    'post_type'      => 'page',
                    'post_excerpt'   => $this->replaceTagWithValue($tagValue['tag'], $tagValue['value'], $destExcerpt),
                    'post_parent'    => $parentPostId === null ? 0 : $parentPostId
                    //'meta_input'     => $postMeta
                ));

                if ($isCopyMetaData) {
                    $postMeta = get_post_custom($this->sourcePageId);
                    foreach ($postMeta as $key => $values) {
                        foreach ($values as $value) {
                            add_post_meta($pageId, $key, $value);
                        }
                    }
                }
                return $pageId;
            } else {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Create sitemap page
     *
     * @param Array $destination Destination info
     *
     * @return Integer|null Page id created or null if failed
     */
    public function createSitemapPage($destination)
    {
        try {
            $dest = is_array($destination) ? $destination : [];
            $destPrefix = array_key_exists('sitemap-prefix', $dest) ? $dest['sitemap-prefix'] : 'sitemap-';
            $destTitle = $destPrefix . $dest['keyword'];
            $destPageName = $destPrefix . $dest['keyword'];
            $destStatus = array_key_exists('status', $dest) ? $dest['status'] : 'publish';

            //$checkDest = get_page_by_title($destTitle, OBJECT, 'page');  // @todo  2019-10-06  MJL  Option to skip creation of page by title?
            $checkDest = null;
            if (empty($checkDest)) {
                $newContent = '';  // Initially empty content - we'll update it after we create all of the pages

                $pageId = wp_insert_post(array(
                    'ID' => 0,
                    'comment_status' => 'closed',
                    'ping_status'    => 'closed',
                    //'post_author'    => 1,
                    'post_title'     => $destTitle,
                    'post_name'      => $destPageName,
                    'post_status'    => $destStatus,
                    'post_content'   => $newContent,
                    'post_type'      => 'page'
                    //'post_excerpt'   => $this->replaceTagWithValue($tagValue['tag'], $tagValue['value'], $destExcerpt),
                ));
                return $pageId;
            } else {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update sitemap page content with list of pages we created
     *
     * @param Integer $sitemapPageId Page id of sitemap
     * @param Array   $pageIds       Array of page ids that we created
     *
     * @return Null
     */
    public function updateSitemapPage($sitemapPageId, $pageIds)
    {
        try {
            $newContent = '';
            foreach ($pageIds as $pageId) {
                $title = get_the_title($pageId);
                $url = get_permalink($pageId);
                if ($url !== false) {
                    $newContent .= '<a href="' . $url . '">' . $title . '<br>';
                }
            }
            $post = array(
                'ID'           => $sitemapPageId,
                'post_content' => $newContent
            );
            wp_update_post($post);

            return null;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Replace tag/value pair in given string
     *
     * @param String $tag
     * @param String $value
     * @param String $subject
     *
     * @return String
     */
    private function replaceTagWithValue($tag, $value, $subject)
    {
        try {
            return preg_replace('/' . $tag . '/', $value, $subject);
        } catch (Exception $e) {
            // Any errors, just return the subject
            return $subject;
        }
    }

    public function test()
    {
        return $this->testData;
        //return print_r($this->sourcePage, true);
    }
}